# Styles showcase

This is a gallery of formatting and blog translation behavior.  It is not meant to be medical or legal advice.

<!-- toc -->

## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

Paragraph text looks like this.  With a little more length it will line wrap and perhaps even do other things.  This is an `inline code block`.  This is ~~strikethrough~~.  This is a [link](https://en.wikipedia.org/).  Finishing **up** with a _little_ more text.

* Bullet points
* More bullet points
* And some "quoted text"

This is a python example from wikipedia:

```python
n = int(input('Type a number, and its factorial will be printed: '))

if n < 0:
    raise ValueError('You must enter a non negative integer')

fact = 1

for i in range(2, n + 1):
    fact *= i

print(fact)
```

A whole file:

<!-- code ../license.txt -->

A cut of a file:

<!-- code_icut ../readme.md 2 5 -->

A cut by delimiters of a file:

<!-- code_cut ../readme.md Features Conventions -->

[Intra-page link](#heading-5)

[Inter-page link](page17.md)

[Link to file](../license.txt)

I hope this leaves you feeling textual.

1. Feelings you are left with
2. Feelings you are not left with
3. Feelings you desire