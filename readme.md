# Somewords

A zero boilerplate, zero configuration static markdown blog generator.

Check it out at <https://rendaw.gitlab.io/blog/>

Or look at the test case/style gallery at <https://rendaw.gitlab.io/somewords/>

## Features

* Subdued
* Brandless
* Zero

## Conventions

* You have a directory of markdown files
* The markdown files are all in git
* Optionally: you have a file named `license.*` in the project root or markdown directory
* Optionally: You're generating your blog in Github or Gitlab pipelines

## Usage

1. Use docker image `registry.gitlab.com/rendaw/somewords:0.1.13` or clone and `cargo build`
2. Run `somewords 'My Blog' path/to/directory/of/markdown/files`
3. Your complete website is in `built/`

### In a Gitlab pipeline

```
image: registry.gitlab.com/rendaw/somewords:0.1.13

pages:
  only:
    - master
  stage: deploy
  script:
    - somewords "My Blog" .
    - mv built public
  artifacts:
    paths:
      - public
```

## Extensions

All these are optional and perhaps not even recommended.

* Add `<!-- toc -->` to get a table of contents.  Capitalization and spacing are important.
* Add `<!-- tags TAG A ^ TAG B ^ TAG C -->` to group posts by tags
* Add `<!-- code path/to/file -->` to make a code block with a file's contents
* Add `<!-- code_cut path/to/file START STOP -->` to include lines between lines containing `START` and `STOP` in a code block
* Add `<!-- code_icut path/to/file 5 10 -->` to include lines [5, 10) in a code block
* Name a file `index.md` to make it the index
* Name files `pin_*.md` to add them to the header (alphabetic order)
* Add a file named `index.js` to `built/` to add javascript
* Replace `index.css` in `built/` to replace `index.css`
