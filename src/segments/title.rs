use crate::common::{write_doc_title, BlogContext, DocMeta};
use crate::segments::common::{SegmentTrait, WritableSegment};
use crate::segments::plain::PlainSegment;
use pulldown_cmark::escape::{escape_href, escape_html, StrWrite};
use std::fs::File;
use std::io::Write;

pub(crate) struct TitleSegment {
    pub(crate) text: String,
    pub(crate) plain_text: String,
}

impl TitleSegment {
    pub fn new() -> TitleSegment {
        return TitleSegment {
            text: "".to_string(),
            plain_text: "".to_string(),
        };
    }
}

impl WritableSegment for TitleSegment {
    fn write(&mut self, text: &str) -> &mut dyn WritableSegment {
        self.text.write_str(text).unwrap();
        return self;
    }

    fn write_content(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_html(&mut self.text, text).unwrap();
        escape_html(&mut self.plain_text, text).unwrap();
        return self;
    }

    fn write_attr(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_href(&mut self.text, text).unwrap();
        return self;
    }
}

const CSS_ELEMENT_TITLE: &'static str = "title";

impl SegmentTrait for TitleSegment {
    fn render(&self, _context: &BlogContext, doc: Option<&DocMeta>, out: &mut File) {
        let doc = doc.unwrap();
        let mut seg = PlainSegment::new();
        seg.t("div", &[CSS_ELEMENT_TITLE], &[]);
        seg.t("h1", &[], &[])
            .write(doc.title.borrow().as_str())
            .et("h1");
        write_doc_title(&mut seg, doc, true);
        seg.et("div");
        out.write_all(seg.text.as_bytes()).unwrap();
    }
}
