use std::cell::RefCell;
use std::fs::File;
use std::io::Write;
use std::rc::Rc;

use crate::common::{
    BlogContext, DocMeta, TOCIndices, CSS_DEPTH, CSS_ELEMENT_HEADER_INDICES,
    CSS_ELEMENT_HEADER_INDICES_ITEM, CSS_ELEMENT_HEADER_TEXT, CSS_TOC, CSS_TOC_ITEM, CSS_TOC_LEVEL,
    TOC,
};
use crate::segments::common::{SegmentTrait, WritableSegment};
use crate::segments::plain::PlainSegment;

pub(crate) struct TOCSegment {}

impl TOCSegment {
    fn render_walk(
        &self,
        doc: &DocMeta,
        element: &Rc<RefCell<TOC>>,
        temp: &mut PlainSegment,
        depth: usize,
    ) {
        temp.t("div", &[CSS_TOC_ITEM], &[]);

        temp.t("span", &[CSS_ELEMENT_HEADER_INDICES], &[]);
        for i in element.indices() {
            temp.t("span", &[CSS_ELEMENT_HEADER_INDICES_ITEM], &[])
                .write(&i.to_string())
                .et("span");
        }
        temp.et("span");

        temp.t(
            "a",
            &[CSS_ELEMENT_HEADER_TEXT],
            &[(
                "href",
                &format!("#{}/{}", doc.slug(), &element.as_ref().borrow().plain_text),
            )],
        );
        temp.write(&element.as_ref().borrow().text).et("a");

        temp.et("div");

        if element.as_ref().borrow().children.len() > 0 {
            let css_depth = format!("{}{}", CSS_DEPTH, depth);
            temp.t("div", &[CSS_TOC_LEVEL, &css_depth], &[]);
            for child in &element.as_ref().borrow().children {
                self.render_walk(doc, child, temp, depth + 1);
            }
            temp.et("div");
        }
    }
}

impl SegmentTrait for TOCSegment {
    fn render(&self, _context: &BlogContext, doc: Option<&DocMeta>, out: &mut File) {
        let doc = doc.unwrap();
        let mut temp = PlainSegment::new();
        temp.t("div", &[CSS_TOC, CSS_TOC_LEVEL], &[]);
        for top in &doc.toc.borrow().children {
            self.render_walk(doc, top, &mut temp, 1);
        }
        temp.et("div");
        out.write_all(temp.text.as_bytes()).unwrap();
    }
}
