use crate::segments::common::{SegmentTrait, WritableSegment};
use pulldown_cmark::escape::{escape_href, escape_html};
use std::fs::File;
use std::io::Write;
use crate::common::{BlogContext, DocMeta};

pub(crate) struct PlainSegment {
    pub text: String,
}

impl PlainSegment {
    pub(crate) fn new() -> PlainSegment {
        return PlainSegment {
            text: "".to_string(),
        };
    }
}

impl WritableSegment for PlainSegment {
    fn write(&mut self, text: &str) -> &mut dyn WritableSegment {
        self.text.push_str(text);
        return self;
    }

    fn write_content(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_html(&mut self.text, text).unwrap();
        return self;
    }

    fn write_attr(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_href(&mut self.text, text).unwrap();
        return self;
    }
}

impl SegmentTrait for PlainSegment {
    fn render(&self, _context: &BlogContext, _doc: Option<&DocMeta>, out: &mut File) {
        out.write_all(self.text.as_bytes()).unwrap();
    }
}
