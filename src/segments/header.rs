use crate::common::{BlogContext, DocMeta, CSS_ELEMENT_HEADER_INDICES, CSS_ELEMENT_HEADER_INDICES_ITEM, CSS_ELEMENT_HEADER_TEXT, TOC, TOCIndices};
use crate::segments::common::{SegmentTrait, WritableSegment};
use crate::segments::plain::PlainSegment;
use pulldown_cmark::escape::{escape_href, escape_html, StrWrite};
use std::cell::RefCell;
use std::fs::File;
use std::io::Write;
use std::rc::Rc;

pub(crate) struct HeaderSegment {
    pub(crate) toc: Rc<RefCell<TOC>>,
    pub(crate) depth: u8,
}

impl WritableSegment for HeaderSegment {
    fn write(&mut self, text: &str) -> &mut dyn WritableSegment {
        self.toc.as_ref().borrow_mut().text.write_str(text).unwrap();
        return self;
    }

    fn write_content(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_html(&mut self.toc.as_ref().borrow_mut().text, text).unwrap();
        self.toc
            .as_ref()
            .borrow_mut()
            .plain_text
            .write_str(text)
            .unwrap();
        return self;
    }

    fn write_attr(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_href(&mut self.toc.as_ref().borrow_mut().text, text).unwrap();
        return self;
    }
}

impl SegmentTrait for HeaderSegment {
    fn render(&self, _context: &BlogContext, doc: Option<&DocMeta>, out: &mut File) {
        let doc = doc.unwrap();
        let mut temp = PlainSegment::new();

        temp.t(
            "a",
            &[],
            &[
                (
                    "name",
                    &format!("{}/{}", doc.slug(), self.toc.as_ref().borrow().slug()),
                ),
                (
                    "href",
                    &format!("#{}/{}", doc.slug(), self.toc.as_ref().borrow().slug()),
                ),
            ],
        );
        let el = format!("h{}", self.depth);
        temp.t(&el, &[], &[]);

        temp.t("span", &[CSS_ELEMENT_HEADER_INDICES], &[]);
        for i in self.toc.indices() {
            temp.t("span", &[CSS_ELEMENT_HEADER_INDICES_ITEM], &[])
                .write(&i.to_string())
                .et("span");
        }
        temp.et("span");

        temp.t("span", &[CSS_ELEMENT_HEADER_TEXT], &[])
            .write(&self.toc.as_ref().borrow().text)
            .et("span");
        temp.et(&el);
        temp.et("a");
        out.write_all(temp.text.as_bytes()).unwrap();
    }
}
