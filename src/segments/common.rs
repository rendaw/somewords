use crate::common::{BlogContext, DocMeta};
use crate::segments::attr::AttrSegment;
use crate::segments::header::HeaderSegment;
use crate::segments::link::LinkSegment;
use crate::segments::plain::PlainSegment;
use crate::segments::title::TitleSegment;
use crate::segments::toc::TOCSegment;
use enum_dispatch::enum_dispatch;
use std::fs::File;

#[enum_dispatch]
pub(crate) trait SegmentTrait {
    fn render(&self, context: &BlogContext, doc: Option<&DocMeta>, out: &mut File);
}

pub(crate) trait WritableSegment {
    fn write(&mut self, text: &str) -> &mut dyn WritableSegment;
    fn write_content(&mut self, text: &str) -> &mut dyn WritableSegment;
    fn write_attr(&mut self, text: &str) -> &mut dyn WritableSegment;

    fn t(
        &mut self,
        tag: &str,
        classes: &[&str],
        attributes: &[(&str, &str)],
    ) -> &mut dyn WritableSegment {
        self.write("<").write(tag);
        if !classes.is_empty() {
            self.write(" class=\"");
            for c in classes {
                self.write(" ").write_attr(*c);
            }
            self.write("\"");
        }
        for attr in attributes {
            self.write(" ")
                .write(attr.0)
                .write("=\"")
                .write_attr(attr.1)
                .write("\"");
        }
        self.write(">")
    }

    fn et(&mut self, tag: &str) -> &mut dyn WritableSegment {
        self.write("</").write(tag).write(">")
    }
}

#[enum_dispatch(SegmentTrait)]
pub(crate) enum Segment {
    PlainSegment(PlainSegment),
    TOCSegment(TOCSegment),
    HeaderSegment(HeaderSegment),
    TitleSegment(TitleSegment),
    AttrSegment(AttrSegment),
    LinkSegment(LinkSegment),
}

impl Segment {
    pub(crate) fn writer(&mut self) -> &mut dyn WritableSegment {
        match self {
            Segment::PlainSegment(e) => e,
            Segment::HeaderSegment(e) => e,
            Segment::TitleSegment(e) => e,
            Segment::AttrSegment(e) => e,
            Segment::LinkSegment(e) => e,
            _ => panic!(),
        }
    }
}
