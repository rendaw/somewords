use crate::segments::common::{SegmentTrait, WritableSegment};
use pulldown_cmark::escape::escape_href;
use std::fs::File;
use std::io::Write;
use crate::common::{BlogContext, DocMeta};

pub(crate) struct AttrSegment {
    text: String,
}

impl AttrSegment {
    pub(crate) fn new() -> AttrSegment {
        AttrSegment {
            text: "".to_string(),
        }
    }
}

impl WritableSegment for AttrSegment {
    fn write(&mut self, text: &str) -> &mut dyn WritableSegment {
        self.text.push_str(text);
        return self;
    }

    fn write_content(&mut self, _text: &str) -> &mut dyn WritableSegment {
        panic!();
    }

    fn write_attr(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_href(&mut self.text, text).unwrap();
        return self;
    }
}

impl SegmentTrait for AttrSegment {
    fn render(&self, _context: &BlogContext, _doc: Option<&DocMeta>, out: &mut File) {
        out.write_all(self.text.as_bytes()).unwrap();
    }
}
