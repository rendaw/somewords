use crate::ci::ci_commit_file_url;
use crate::common::{is_offsite, BlogContext, DocMeta, copy_missing};
use crate::segments::common::{SegmentTrait, WritableSegment};
use crate::segments::plain::PlainSegment;
use pulldown_cmark::escape::{escape_html, StrWrite};
use std::fs::File;
use std::path::Path;

pub(crate) struct LinkSegment {
    dest: String,
    text: String,
}

impl LinkSegment {
    pub(crate) fn new(dest: &str) -> LinkSegment {
        return LinkSegment {
            dest: dest.to_string(),
            text: "".to_string(),
        };
    }
}

impl WritableSegment for LinkSegment {
    fn write(&mut self, text: &str) -> &mut dyn WritableSegment {
        self.text.write_str(text).unwrap();
        return self;
    }

    fn write_content(&mut self, text: &str) -> &mut dyn WritableSegment {
        escape_html(&mut self.text, text).unwrap();
        return self;
    }

    fn write_attr(&mut self, _text: &str) -> &mut dyn WritableSegment {
        panic!();
    }
}

impl SegmentTrait for LinkSegment {
    fn render(&self, context: &BlogContext, doc: Option<&DocMeta>, out: &mut File) {
        let doc = doc.unwrap();
        let mut seg = PlainSegment::new();
        seg.write("<a href=\"");
        if is_offsite(&self.dest) {
            seg.write_attr(&self.dest);
        } else if self.dest.starts_with("#") {
            seg.write_attr(&format!("#{}/{}", doc.slug(), &self.dest[1..]));
        } else if self.dest.starts_with("/") {
            // Based on where the output is placed on the server relative to the url root this could be anything
            seg.write_attr(&self.dest);
        } else {
            let repo_path = Path::new(&doc.repo_path).parent().unwrap().join(&self.dest);
            if self.dest.ends_with(".md")
                && context
                    .doc_lookup
                    .get(repo_path.to_str().unwrap())
                    .map(|doc| {
                        seg.write_attr(&doc.out_url_filename());
                        ()
                    })
                    .is_some()
            {
                // done
            } else {
                match ci_commit_file_url(repo_path.to_str().unwrap()) {
                    Some(url) => {
                        seg.write_attr(&url);
                    }
                    None => {
                        if repo_path.starts_with(&context.source_dir) {
                            copy_missing(context, &repo_path);
                        }
                        seg.write_attr(&self.dest);
                    }
                };
            };
        }
        seg.write("\">");
        seg.write_content(&self.text);
        seg.write("</a>");
        seg.render(context, None, out);
    }
}
