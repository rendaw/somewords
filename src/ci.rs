fn gitlab_ci_env() -> Option<(String, String)> {
    std::env::var("CI_PROJECT_URL")
        .ok() /* gitlab */
        .and_then(|base| std::env::var("CI_COMMIT_SHA").ok().map(|sha| (base, sha)))
}

fn git_ci_env() -> Option<(String, String)> {
    std::env::var("GITHUB_REPOSITORY")
        .ok()
        .and_then(|base| std::env::var("GITHUB_SHA").ok().map(|sha| (base, sha)))
}

pub fn ci_commit_url() -> Option<String> {
    gitlab_ci_env()
        .map(|(base, sha)| format!("{}/-/commit/{}", base, sha))
        .or_else(|| {
            git_ci_env().map(|(base, sha)| format!("https://github.com/{}/commit/{}", base, sha))
        })
}

pub fn ci_commit_file_url(path: &str) -> Option<String> {
    gitlab_ci_env()
        .map(|(base, sha)| format!("{}/-/blob/{}/{}", base, sha, path))
        .or_else(|| {
            git_ci_env()
                .map(|(base, sha)| format!("https://github.com/{}/blob/{}/{}", base, sha, path))
        })
}
