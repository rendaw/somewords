use std::borrow::Cow;
use std::cell::RefCell;
use std::cmp::min;
use std::collections::HashMap;
use std::env::{args, current_dir};
use std::fs;
use std::fs::{create_dir_all, read_dir, File};
use std::io::{BufRead, BufReader, Read, Write};
use std::path::{Path, PathBuf};
use std::process::Command;
use std::rc::Rc;
use std::str::{from_utf8, FromStr};

use chrono::DateTime;
use num_integer::Integer;
use pulldown_cmark::{CodeBlockKind, Event, LinkType, Options, Parser, Tag};

use crate::ci::{ci_commit_file_url, ci_commit_url};
use crate::common::{
    copy_missing, is_offsite, write_doc_title, BlogContext, DocMeta, CSS_BLOCK, CSS_DOC_LIST,
    CSS_DOC_LIST_ITEM, CSS_ELEMENT_TITLE, CSS_HEADER_BLOG_TITLE, CSS_HEADER_LINK,
    CSS_HEADER_LINK_AUTHORS, CSS_HEADER_LINK_HOME, CSS_HEADER_LINK_PIN, CSS_HEADER_LINK_PIN_PREFIX,
    CSS_HEADER_LINK_POSTS, CSS_HEADER_LINK_TAGS, CSS_LINK_LICENSE, CSS_LINK_SOURCE, CSS_LIST,
    CSS_LIST_ITEM_LINK, CSS_PAGE_AUTHORS, CSS_PAGE_AUTHORS_PAGE, CSS_PAGE_DOC, CSS_PAGE_INDEX,
    CSS_PAGE_PAGE, CSS_PAGE_PIN, CSS_PAGE_POSTS_PAGE, CSS_PAGE_SLUG_PREFIX, CSS_PAGE_TAGS,
    CSS_PAGE_TAGS_PAGE, CSS_PAGINATOR, CSS_PAGINATOR_ITEM, CSS_PAGINATOR_ITEM_NEXT,
    CSS_PAGINATOR_ITEM_PREV, CSS_SECTION_BODY, CSS_SECTION_FOOTER, CSS_SECTION_HEADER,
    CSS_SYNTAX_PREFIX, CSS_TAGS_LIST, CSS_TAGS_LIST_ITEM_LINK, TOC,
};
use crate::segments::attr::AttrSegment;
use crate::segments::common::{Segment, SegmentTrait, WritableSegment};
use crate::segments::header::HeaderSegment;
use crate::segments::link::LinkSegment;
use crate::segments::plain::PlainSegment;
use crate::segments::title::TitleSegment;
use crate::segments::toc::TOCSegment;

mod ci;
mod common;
mod segments;

const META_TAGS_PREFIX: &'static str = "<!-- tags ";
const META_SUFFIX: &'static str = " -->";
const META_CODE_PREFIX: &'static str = "<!-- code ";
const META_CODECUT_PREFIX: &'static str = "<!-- code_cut ";
const META_CODEICUT_PREFIX: &'static str = "<!-- code_icut ";

fn process_article(context: &BlogContext, doc: &DocMeta) -> Vec<Segment> {
    // State
    let mut segments: Vec<Segment> = Vec::new();
    segments.push(Segment::from(PlainSegment::new()));

    let mut toc_stack: Vec<Rc<RefCell<TOC>>> = vec![doc.toc.clone()];

    // Parse doc
    let mut file = File::open(context.repo_dir.join(&doc.repo_path).to_str().unwrap()).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    drop(file);
    let mut parser_options = Options::empty();
    parser_options.insert(Options::ENABLE_STRIKETHROUGH);
    parser_options.insert(Options::ENABLE_TABLES);
    parser_options.insert(Options::ENABLE_SMART_PUNCTUATION);
    let parser = Parser::new_ext(&contents, parser_options);

    for event in parser {
        let seg = segments.last_mut().unwrap().writer();
        match event {
            Event::Start(e) => {
                match &e {
                    Tag::Paragraph => {
                        seg.t("p", &[], &[]);
                    }
                    Tag::Heading(t) => {
                        drop(seg);
                        if *t == 1 {
                            segments.push(Segment::from(TitleSegment::new()));
                        } else {
                            while *t <= toc_stack.len() as u32 {
                                toc_stack.pop().unwrap();
                            }
                            let toc_entry = TOC::new(
                                toc_stack.last().unwrap().as_ref().borrow().children.len() + 1,
                                Some(toc_stack.last().unwrap().clone()),
                            );
                            toc_stack
                                .last_mut()
                                .unwrap()
                                .as_ref()
                                .borrow_mut()
                                .children
                                .push(toc_entry.clone());
                            toc_stack.push(toc_entry.clone());
                            segments.push(Segment::from(HeaderSegment {
                                toc: toc_entry.clone(),
                                depth: *t as u8,
                            }));
                        }
                    }
                    Tag::BlockQuote => {
                        seg.t("blockquote", &[], &[]);
                    }
                    Tag::CodeBlock(t) => {
                        seg.t(
                            "code",
                            &[
                                CSS_BLOCK,
                                &format!(
                                    "{}{}",
                                    CSS_SYNTAX_PREFIX,
                                    match t {
                                        CodeBlockKind::Indented => {
                                            Option::None
                                        }
                                        CodeBlockKind::Fenced(f) => {
                                            Option::Some(f.as_ref())
                                        }
                                    }
                                    .unwrap_or("plain")
                                ),
                            ],
                            &[],
                        );
                    }
                    Tag::List(t) => {
                        match t {
                            None => seg.t("ul", &[], &[]),
                            Some(start) => seg.t("ol", &[], &[("start", &start.to_string())]),
                        };
                    }
                    Tag::Item => {
                        seg.t("li", &[], &[]);
                    }
                    Tag::FootnoteDefinition(_t) => {
                        // TODO
                    }
                    Tag::Table(_t) => {
                        seg.write("<table><tbody>\n");
                    }
                    Tag::TableHead => {
                        seg.t("th", &[], &[]);
                    }
                    Tag::TableRow => {
                        seg.t("tr", &[], &[]);
                    }
                    Tag::TableCell => {
                        seg.t("td", &[], &[]);
                    }
                    Tag::Emphasis => {
                        seg.t("em", &[], &[]);
                    }
                    Tag::Strong => {
                        seg.t("strong", &[], &[]);
                    }
                    Tag::Strikethrough => {
                        seg.t("strike", &[], &[]);
                    }
                    Tag::Link(t, target, _title) => {
                        drop(seg);
                        match t {
                            LinkType::Email => {
                                segments.push(Segment::from(LinkSegment::new(&format!(
                                    "mailto:{}",
                                    target
                                ))));
                            }
                            _ => {
                                segments.push(Segment::from(LinkSegment::new(target)));
                            }
                        }
                    }
                    Tag::Image(_t, target, _title) => {
                        seg.write("<img src=\"");
                        if is_offsite(target) {
                            seg.write_attr(target);
                        } else if target.starts_with("/") {
                            seg.write_attr(target);
                        } else {
                            let repo_path = Path::new(&doc.repo_path)
                                .parent()
                                .unwrap()
                                .join(target.as_ref());
                            let source_path = repo_path.strip_prefix(&context.source_dir).unwrap();
                            let out_path = context.output_dir.join(&source_path);
                            if !fs::metadata(&out_path).is_ok() {
                                fs::copy(context.repo_dir.join(&repo_path), &out_path).unwrap();
                            }
                            seg.write_attr(&target);
                        }
                        seg.write("\" alt=\"");
                        drop(seg);
                        segments.push(Segment::from(AttrSegment::new()));
                    }
                };
            }
            Event::End(e) => {
                match e {
                    Tag::Paragraph => {
                        seg.et("p");
                    }
                    Tag::Heading(t) => {
                        drop(seg);
                        if t == 1 {
                            match segments.last().unwrap() {
                                Segment::TitleSegment(e) => {
                                    if doc.title.borrow().is_empty() {
                                        doc.title.replace(e.text.clone());
                                        doc.plain_title.replace(e.plain_text.clone());
                                    }
                                }
                                _ => panic!(),
                            };
                        }
                        segments.push(Segment::from(PlainSegment::new()));
                    }
                    Tag::BlockQuote => {
                        seg.et("blockquote");
                    }
                    Tag::CodeBlock(_t) => {
                        seg.et("code");
                    }
                    Tag::List(t) => {
                        match t {
                            None => seg.et("ul"),
                            Some(_start) => seg.et("ol"),
                        };
                    }
                    Tag::Item => {
                        seg.et("li");
                    }
                    Tag::FootnoteDefinition(_t) => {
                        // TODO
                    }
                    Tag::Table(_t) => {
                        seg.et("tbody").et("table");
                    }
                    Tag::TableHead => {
                        seg.et("th");
                    }
                    Tag::TableRow => {
                        seg.et("tr");
                    }
                    Tag::TableCell => {
                        seg.et("td");
                    }
                    Tag::Emphasis => {
                        seg.et("em");
                    }
                    Tag::Strong => {
                        seg.et("strong");
                    }
                    Tag::Strikethrough => {
                        seg.et("strike");
                    }
                    Tag::Link(_t, _dest, _title) => {
                        drop(seg);
                        segments.push(Segment::from(PlainSegment::new()));
                    }
                    Tag::Image(_t, _dest, _title) => {
                        seg.write("\"/>\n");
                        drop(seg);
                        segments.push(Segment::from(PlainSegment::new()));
                    }
                }
            }
            Event::Text(e) => {
                seg.write_content(e.as_ref());
            }
            Event::Code(e) => {
                seg.t("code", &["inline"], &[])
                    .write_content(e.as_ref())
                    .et("code");
            }
            Event::Html(e) => {
                fn cut_meta(prefix: &str, suffix: &str, text: &str) -> Vec<String> {
                    text[prefix.len()..text.len() - suffix.len() - 1]
                        .split(" ")
                        .into_iter()
                        .map(|v| v.trim().to_string())
                        .collect()
                }

                if e.as_ref().starts_with("<!-- toc -->") {
                    segments.push(Segment::from(TOCSegment {}));
                    segments.push(Segment::from(PlainSegment::new()));
                } else if e.as_ref().starts_with(META_CODE_PREFIX) {
                    let args = cut_meta(META_CODE_PREFIX, META_SUFFIX, e.as_ref());
                    if args.len() != 1 {
                        panic!(
                            "Code reference in {} needs 1 argument: the path to the target file",
                            doc.repo_path.to_str().unwrap()
                        );
                    }
                    let path = doc.repo_path.parent().unwrap().join(&args[0]);
                    let mut file = File::open(&path)
                        .map_err(|e| {
                            panic!(
                                "Coudln't open code reference target {}: {}",
                                path.to_str().unwrap(),
                                e
                            )
                        })
                        .unwrap();
                    let mut buf = "".to_string();
                    file.read_to_string(&mut buf).unwrap();
                    seg.t("code", &[CSS_BLOCK], &[])
                        .write_content(&buf)
                        .et("code");
                } else if e.as_ref().starts_with(META_CODECUT_PREFIX) {
                    let args = cut_meta(META_CODECUT_PREFIX, META_SUFFIX, e.as_ref());
                    if args.len() != 3 {
                        panic!("Labeled code reference in {} needs 3 arguments: file, start label, end label", doc.repo_path.to_str().unwrap());
                    }
                    let path = doc.repo_path.parent().unwrap().join(&args[0]);
                    let file = File::open(&path)
                        .map_err(|e| {
                            panic!(
                                "Coudln't open code reference target {}: {}",
                                path.to_str().unwrap(),
                                e
                            )
                        })
                        .unwrap();
                    let reader = BufReader::new(file);
                    enum State {
                        BEFORE,
                        IN,
                        AFTER,
                    }
                    let mut state = State::BEFORE;
                    seg.t("code", &[CSS_BLOCK], &[]);
                    for line in reader.lines() {
                        let line = line.unwrap();
                        match &state {
                            State::BEFORE => {
                                if line.contains(&args[1]) {
                                    state = State::IN;
                                }
                            }
                            State::IN => {
                                if line.contains(&args[2]) {
                                    state = State::AFTER;
                                    break;
                                } else {
                                    seg.write_content(&line).write("\n");
                                }
                            }
                            State::AFTER => {}
                        }
                    }
                    match state {
                        State::AFTER => {}
                        State::BEFORE => {
                            panic!(
                                "Start delimiter [{}] for code reference {} not found!",
                                &args[1],
                                path.to_str().unwrap()
                            );
                        }
                        State::IN => {
                            panic!(
                                "End delimiter [{}] for code reference {} not found!",
                                &args[2],
                                path.to_str().unwrap()
                            );
                        }
                    };
                    seg.et("code");
                } else if e.as_ref().starts_with(META_CODEICUT_PREFIX) {
                    let args = cut_meta(META_CODEICUT_PREFIX, META_SUFFIX, e.as_ref());
                    if args.len() != 3 {
                        panic!("Indexed code reference in {} needs 3 arguments: file, start line (inc), end line (exc)", doc.repo_path.to_str().unwrap());
                    }
                    let path = doc.repo_path.parent().unwrap().join(&args[0]);
                    let start = usize::from_str(&args[1])
                        .map_err(|_| {
                            panic!(
                                "Start index {} for code reference {} is not a valid number",
                                &args[1], &args[0]
                            )
                        })
                        .unwrap();
                    let end = usize::from_str(&args[2])
                        .map_err(|_| {
                            panic!(
                                "End index {} for code reference {} is not a valid number",
                                &args[2], &args[0]
                            )
                        })
                        .unwrap();
                    let file = File::open(&path)
                        .map_err(|e| {
                            panic!(
                                "Coudln't open code reference target {}: {}",
                                path.to_str().unwrap(),
                                e
                            )
                        })
                        .unwrap();
                    let reader = BufReader::new(file);
                    enum State {
                        BEFORE,
                        IN,
                        AFTER,
                    }
                    let mut state = State::BEFORE;
                    seg.t("code", &[CSS_BLOCK], &[]);
                    for (i, line) in reader.lines().enumerate() {
                        let line = line.unwrap();
                        match &state {
                            State::BEFORE => {
                                if i == start {
                                    state = State::IN;
                                }
                            }
                            State::IN => {
                                if i == end {
                                    state = State::AFTER;
                                    break;
                                }
                            }
                            State::AFTER => {}
                        }
                        match &state {
                            State::IN => {
                                seg.write_content(&line).write("\n");
                            }
                            _ => (),
                        }
                    }
                    match state {
                        State::AFTER => {}
                        State::BEFORE => {
                            panic!(
                                "Start delimiter [{}] for indexed code reference {} not found!",
                                &args[1],
                                path.to_str().unwrap()
                            );
                        }
                        State::IN => {
                            panic!(
                                "End delimiter [{}] for indexed code reference {} not found!",
                                &args[2],
                                path.to_str().unwrap()
                            );
                        }
                    };
                    seg.et("code");
                } else if e.starts_with(META_TAGS_PREFIX) {
                    for tag in e.as_ref()[META_TAGS_PREFIX.len()..e.len() - META_SUFFIX.len() - 1]
                        .split("^")
                    {
                        let tag = tag.trim();
                        doc.tags.borrow_mut().push(tag.into());
                    }
                } else {
                    seg.write(&e);
                }
            }
            Event::FootnoteReference(_e) => {
                // TODO
            }
            Event::SoftBreak => {
                seg.write("\n");
            }
            Event::HardBreak => {
                seg.t("br", &[], &[]);
            }
            Event::Rule => {
                seg.t("hr", &[], &[]);
            }
            Event::TaskListMarker(_e) => {
                // TODO
            }
        }
    }

    return segments;
}

const INDEX_FILENAME: &'static str = "index.html";
const TAGS_FILENAME: &'static str = "tags.html";
const AUTHORS_FILENAME: &'static str = "authors.html";

fn reverse(p: &Path) -> PathBuf {
    let mut out = PathBuf::new();
    for _ in 0..p.ancestors().count() - 1 {
        out.push("..");
    }
    return out;
}

fn write_html<F>(
    path: &Path,
    context: &BlogContext,
    doc: Option<&DocMeta>,
    type_tags: &[&str],
    title: &str,
    body: F,
) where
    F: Fn(&mut File) -> (),
{
    let rel_root = reverse(path.parent().unwrap());
    let mut seg = PlainSegment::new();
    seg.write("<!doctype html>\n");
    seg.t("html", &[], &[]);
    seg.t("head", &[], &[]);
    seg.t("meta", &[], &[("charset", "utf-8")]);
    seg.t(
        "meta",
        &[],
        &[
            ("name", "viewport"),
            ("content", "width=device-width, initial-scale=1.0"),
        ],
    );
    seg.t("title", &[], &[]);
    seg.write_content(&format!("{} - {}", &context.title, &title));
    seg.et("title");
    seg.t(
        "link",
        &[],
        &[
            ("rel", "stylesheet"),
            ("href", rel_root.join("index.css").to_str().unwrap()),
        ],
    );
    seg.et("head");
    seg.t("body", type_tags, &[]);

    seg.t("div", &[CSS_SECTION_HEADER], &[]);

    for i in &context.index {
        seg.t(
            "a",
            &[CSS_HEADER_LINK, CSS_HEADER_LINK_HOME, CSS_HEADER_BLOG_TITLE],
            &[("href", rel_root.join(INDEX_FILENAME).to_str().unwrap())],
        )
        .write(i.title.borrow().as_str())
        .et("a");
    }
    seg.t(
        "a",
        &[CSS_HEADER_LINK, CSS_HEADER_LINK_POSTS],
        &[(
            "href",
            if context.index.is_some() {
                rel_root.join("list").join("1.html")
            } else {
                rel_root.join(INDEX_FILENAME)
            }
            .to_str()
            .unwrap(),
        )],
    )
    .write("Posts")
    .et("a");
    seg.t(
        "a",
        &[CSS_HEADER_LINK, CSS_HEADER_LINK_TAGS],
        &[("href", rel_root.join(TAGS_FILENAME).to_str().unwrap())],
    )
    .write("Tags")
    .et("a");
    seg.t(
        "a",
        &[CSS_HEADER_LINK, CSS_HEADER_LINK_AUTHORS],
        &[("href", rel_root.join(AUTHORS_FILENAME).to_str().unwrap())],
    )
    .write("Authors")
    .et("a");
    for pin in &context.pin {
        seg.t(
            "a",
            &[
                CSS_HEADER_LINK,
                CSS_HEADER_LINK_PIN,
                &format!("{}{}", CSS_HEADER_LINK_PIN_PREFIX, &pin.slug()),
            ],
            &[(
                "href",
                rel_root.join(&pin.out_url_filename()).to_str().unwrap(),
            )],
        )
        .write(pin.title.borrow().as_str())
        .et("a");
    }
    seg.et("div");

    seg.t("div", &[CSS_SECTION_BODY], &[]);

    let abs_path = context.output_dir.join(path);
    create_dir_all(abs_path.parent().unwrap()).unwrap();
    let mut out = File::create(abs_path).unwrap();
    seg.render(context, None, &mut out);
    body(&mut out);

    let mut seg = PlainSegment::new();
    seg.et("div");

    seg.t("div", &[CSS_SECTION_FOOTER], &[]);
    if doc.is_none() {
        for repo in ci_commit_url() {
            seg.t("a", &[CSS_LINK_SOURCE], &[("href", &repo)])
                .write("Source")
                .et("a");
        }
    }

    for license in &context.license_url {
        seg.t(
            "a",
            &[CSS_LINK_LICENSE],
            &[("href", rel_root.join(&license).to_str().unwrap())],
        )
        .write("License")
        .et("a");
    }

    for doc in doc {
        if !doc.tags.borrow().is_empty() {
            seg.t("div", &[CSS_TAGS_LIST], &[]);
            for tag in doc.tags.borrow().iter() {
                seg.t(
                    "a",
                    &[CSS_TAGS_LIST_ITEM_LINK],
                    &[(
                        "href",
                        &format!("{}tags/{}/1.html", rel_root.to_str().unwrap(), tag),
                    )],
                )
                .write_content(tag)
                .et("a");
            }
            seg.et("div");
        }
    }
    seg.et("div");

    seg.t(
        "script",
        &[],
        &[("src", rel_root.join("index.js").to_str().unwrap())],
    )
    .et("script");
    seg.et("body");
    seg.et("html");
    seg.render(context, None, &mut out);
}

fn main() {
    let args: Vec<String> = args().collect();
    if args.len() != 3 {
        panic!("You must specify two arguments, the blog title and the path to the directory containing your markdown files.");
    }
    let title = args[1].to_string();
    let source_dir = Path::new(&args[2])
        .canonicalize()
        .expect("Can't resolve source directory");
    drop(args);

    struct ParentSearch<'a> {
        at: Option<&'a Path>,
    }
    impl<'a> Iterator for ParentSearch<'a> {
        type Item = &'a Path;

        fn next(&mut self) -> Option<Self::Item> {
            match self.at {
                None => None,
                Some(i) => {
                    self.at = i.parent();
                    Some(i)
                }
            }
        }
    }
    let repo_dir = match (ParentSearch {
        at: Some(&source_dir),
    })
    .filter(|d| fs::metadata(&d.join(".git")).is_ok())
    .next()
    {
        None => panic!("Couldn't find git repo root (containing [.git]) for source directory."),
        Some(d) => d.to_owned(),
    };

    let relative_source_dir = source_dir.strip_prefix(&repo_dir).unwrap().to_owned();
    let here = current_dir().unwrap();

    let output_root = here.join("built");
    create_dir_all(&output_root).unwrap();
    {
        let mut css = File::create(output_root.join("index.css")).unwrap();
        css.write_all(include_bytes!("../index.css")).unwrap();
    }

    let files: Vec<_> = read_dir(&source_dir).unwrap().map(|x| x.unwrap()).collect();
    let files: Vec<DocMeta> = files
        .into_iter()
        .filter(|e| {
            e.file_type().unwrap().is_file()
                && e.file_name().into_string().unwrap().ends_with(".md")
        })
        .map(|v| {
            let hash_name = Command::new("git")
                .args(&[
                    "log",
                    "--diff-filter=A",
                    "--follow",
                    "--format=%H",
                    "-1",
                    "--first-parent",
                    "--name-only",
                ])
                .arg(&v.file_name())
                .current_dir(&source_dir)
                .output()
                .expect("Failed to run git; make sure git is installed, among other things.")
                .stdout;
            if hash_name.is_empty() {
                println!(
                    "File {} isn't managed by git, skipping.",
                    v.file_name().to_str().unwrap()
                );
                return None;
            }
            let hash_name: Vec<String> = hash_name
                .split(|c| match c {
                    b' ' => true,
                    b'\n' => true,
                    b'\r' => true,
                    b'\t' => true,
                    _ => false,
                })
                .filter(|s| s.len() > 0)
                .map(|s| from_utf8(s).unwrap().to_string())
                .collect();
            let hash = &hash_name[0];
            let id = hex::encode(
                sha1::Sha1::from(&format!("{} {}", hash_name[0], hash_name[1]))
                    .digest()
                    .bytes(),
            )[..12]
                .to_string();
            let created = DateTime::parse_from_rfc3339(
                &from_utf8(
                    &Command::new("git")
                        .args(&["show", "-s", "--format=%aI"])
                        .arg(hash)
                        .current_dir(&source_dir)
                        .output()
                        .unwrap()
                        .stdout,
                )
                .unwrap()
                .trim(),
            )
            .unwrap();
            let updated = DateTime::parse_from_rfc3339(
                &from_utf8(
                    &Command::new("git")
                        .args(&["log", "-1", "--format=%aI"])
                        .arg(&v.file_name())
                        .current_dir(&source_dir)
                        .output()
                        .unwrap()
                        .stdout,
                )
                .unwrap()
                .trim(),
            )
            .unwrap();
            let name = from_utf8(
                &Command::new("git")
                    .args(&["show", "-s", "--format=%aN"])
                    .arg(hash)
                    .current_dir(&source_dir)
                    .output()
                    .unwrap()
                    .stdout,
            )
            .unwrap()
            .trim()
            .to_string();
            let email = from_utf8(
                &Command::new("git")
                    .args(&["show", "-s", "--format=%aE"])
                    .arg(hash)
                    .current_dir(&source_dir)
                    .output()
                    .unwrap()
                    .stdout,
            )
            .unwrap()
            .trim()
            .to_string();
            let filename = v.file_name().into_string().unwrap();
            let repo_path = relative_source_dir.join(&filename);
            return Some(DocMeta {
                filename,
                repo_path,
                created,
                updated,
                id,
                author_name: name,
                author_email: email,
                title: RefCell::new("".to_string()),
                plain_title: RefCell::new("".to_string()),
                tags: RefCell::new(Vec::new()),
                toc: TOC::new(0, None),
            });
        })
        .filter(Option::is_some)
        .map(|v| v.unwrap())
        .collect();

    // Global state
    let (index, files): (Vec<DocMeta>, Vec<DocMeta>) =
        files.into_iter().partition(|f| f.filename.eq("index.md"));
    let (mut pin, mut docs): (Vec<DocMeta>, Vec<DocMeta>) = files
        .into_iter()
        .partition(|f| f.filename.starts_with("pin_"));
    pin.sort_by_cached_key(|f| f.filename.clone());
    docs.sort_by_cached_key(|f| f.created.clone());
    docs.reverse();

    let mut context = BlogContext {
        doc_lookup: HashMap::new(),
        source_dir,
        repo_dir: repo_dir.into(),
        output_dir: output_root,
        license_url: None,
        title,
        pin: Vec::new(),
        index: None,
        tags: HashMap::new(),
        authors: HashMap::new(),
    };

    for doc in &docs {
        context
            .doc_lookup
            .insert(doc.repo_path.to_str().unwrap().to_string(), doc);
    }

    fn license_search(dir: &Path) -> Option<String> {
        read_dir(dir)
            .unwrap()
            .into_iter()
            .map(|e| {
                let filename = e.unwrap().file_name().to_str().unwrap().to_owned();
                if !filename.to_ascii_lowercase().starts_with("license.") {
                    return None;
                }
                return Some(filename);
            })
            .filter(|e| e.is_some())
            .next()
            .map(|e| e.unwrap())
    }
    context.license_url = license_search(&context.repo_dir)
        .and_then(|p| {
            ci_commit_file_url(&p).or_else(|| {
                fs::copy(&p, context.output_dir.join(&p)).unwrap();
                return Some(p);
            })
        })
        .or_else(|| {
            license_search(&context.source_dir).map(|p| {
                let repo_path = context.source_dir.join(&p);
                copy_missing(&context, &repo_path);
                return repo_path.to_str().unwrap().to_owned();
            })
        });

    // Process files
    let index = index
        .into_iter()
        .map(|mut index| {
            let segments = process_article(&context, &mut index);
            (index, segments)
        })
        .next();

    let pin: Vec<(DocMeta, Vec<Segment>)> = pin
        .into_iter()
        .map(|mut pin| {
            let segments = process_article(&context, &mut pin);
            (pin, segments)
        })
        .collect();

    let pin_segments: Vec<Vec<Segment>> = pin
        .into_iter()
        .map(|e| {
            context.pin.push(e.0);
            e.1
        })
        .collect();
    let index_segments = index.map(|index| {
        context.index = Some(index.0);
        index.1
    });

    for doc in &context.index {
        context.doc_lookup.insert("index.md".to_string(), doc);
    }
    for doc in &context.pin {
        context
            .doc_lookup
            .insert(doc.repo_path.to_str().unwrap().to_string(), doc);
    }

    for index in context.index.iter().zip(index_segments) {
        write_html(
            Path::new(INDEX_FILENAME),
            &context,
            Some(index.0),
            &[
                CSS_PAGE_DOC,
                CSS_PAGE_INDEX,
                &format!("{}{}", CSS_PAGE_SLUG_PREFIX, index.0.slug()),
            ],
            index.0.plain_title.borrow().as_str(),
            |o| {
                for segment in &index.1 {
                    segment.render(&context, Some(index.0), o);
                }
            },
        )
    }

    for pin in context.pin.iter().zip(pin_segments) {
        write_html(
            Path::new(&pin.0.out_filename()),
            &context,
            Some(pin.0),
            &[
                CSS_PAGE_DOC,
                CSS_PAGE_PIN,
                &format!("{}{}", CSS_PAGE_SLUG_PREFIX, pin.0.slug()),
            ],
            pin.0.plain_title.borrow().as_str(),
            |o| {
                for segment in &pin.1 {
                    segment.render(&context, Some(pin.0), o);
                }
            },
        )
    }

    for doc in &docs {
        let segments = process_article(&context, doc);
        write_html(
            Path::new(&format!("{}.html", &doc.id)),
            &context,
            Some(doc),
            &[
                CSS_PAGE_DOC,
                &format!("{}{}", CSS_PAGE_SLUG_PREFIX, doc.slug()),
            ],
            doc.plain_title.borrow().as_str(),
            |o| {
                for segment in &segments {
                    segment.render(&context, Some(&doc), o);
                }
            },
        );
    }

    for doc in &docs {
        context
            .authors
            .entry(doc.author_name.clone())
            .or_insert(Vec::new())
            .push(doc);
        for tag in doc.tags.borrow().iter() {
            context
                .tags
                .entry(tag.clone())
                .or_insert(Vec::new())
                .push(doc);
        }
    }

    let doc_refs = docs.iter().map(|d| d).collect::<Vec<&DocMeta>>();
    write_pages(
        &context,
        match &context.index {
            None => Some(Path::new("index.html")),
            Some(_) => None,
        },
        Path::new("posts"),
        &[CSS_PAGE_PAGE, CSS_PAGE_POSTS_PAGE],
        "Posts",
        &doc_refs,
    );

    write_html(
        Path::new(TAGS_FILENAME),
        &context,
        None,
        &[CSS_PAGE_TAGS],
        "Tags",
        |o| {
            let mut seg = PlainSegment::new();
            seg.t("div", &[CSS_ELEMENT_TITLE], &[]);
            seg.t("h1", &[], &[]).write("Tags").et("h1");
            seg.et("div");
            seg.t("div", &[CSS_LIST], &[]);
            for tag in &context.tags {
                seg.t(
                    "a",
                    &[CSS_LIST_ITEM_LINK],
                    &[("href", &format!("tags/{}/1.html", &tag.0))],
                )
                .write_content(&tag.0)
                .et("a");
            }
            seg.et("div");
            seg.render(&context, None, o);
        },
    );
    for tag in &context.tags {
        write_pages(
            &context,
            None,
            &Path::new("tags").join(&tag.0),
            &[CSS_PAGE_TAGS_PAGE, CSS_PAGE_PAGE],
            tag.0,
            &tag.1,
        );
    }

    write_html(
        Path::new(AUTHORS_FILENAME),
        &context,
        None,
        &[CSS_PAGE_AUTHORS],
        "Authors",
        |o| {
            let mut seg = PlainSegment::new();
            seg.t("div", &[CSS_ELEMENT_TITLE], &[]);
            seg.t("h1", &[], &[]).write("Authors").et("h1");
            seg.et("div");
            seg.t("div", &[CSS_LIST], &[]);
            for author in &context.authors {
                seg.t(
                    "a",
                    &[CSS_LIST_ITEM_LINK],
                    &[("href", &format!("authors/{}/1.html", &author.0))],
                )
                .write_content(&author.0)
                .et("a");
            }
            seg.et("div");
            seg.render(&context, None, o);
        },
    );
    for author in &context.authors {
        write_pages(
            &context,
            None,
            &Path::new("authors").join(&author.0),
            &[CSS_PAGE_AUTHORS_PAGE, CSS_PAGE_PAGE],
            author.0,
            &author.1,
        );
    }
}

fn write_pages(
    context: &BlogContext,
    first_path: Option<&Path>, /* Must be in source root */
    path_base: &Path,
    t: &[&str],
    title: &str,
    docs: &[&DocMeta],
) {
    const PAGE_SIZE: usize = 20;
    let pages = docs.len().div_ceil(&PAGE_SIZE);
    for i in 0..pages {
        let path = match (i, first_path) {
            (0, Some(path)) => Cow::Borrowed(path),
            _ => Cow::Owned(Path::new(path_base).join(&format!("{}.html", i + 1))),
        };
        let slice: &[&DocMeta] = &docs[i * PAGE_SIZE..min((i + 1) * PAGE_SIZE, docs.len())];
        write_html(
            &path,
            context,
            None,
            t,
            &format!("{}, page {}", title, i + 1),
            |out| {
                let mut seg = PlainSegment::new();
                seg.t("div", &[CSS_ELEMENT_TITLE], &[]);
                seg.t("h1", &[], &[]).write(&title).et("h1");
                seg.et("div");
                seg.t("div", &[CSS_DOC_LIST], &[]);
                for doc in slice {
                    seg.t("div", &[CSS_DOC_LIST_ITEM], &[]);
                    seg.t(
                        "a",
                        &[CSS_LIST_ITEM_LINK],
                        &[(
                            "href",
                            match (i, first_path) {
                                (0, Some(_)) => Path::new(&doc.out_url_filename()).to_owned(),
                                _ => reverse(path_base).join(&doc.out_url_filename()),
                            }
                            .to_str()
                            .unwrap(),
                        )],
                    )
                    .write(doc.title.borrow().as_str())
                    .et("a");
                    write_doc_title(&mut seg, doc, false);
                    seg.et("div");
                }
                seg.et("div");
                if pages > 1 {
                    seg.t("div", &[CSS_PAGINATOR], &[]);
                    if i != 0 {
                        seg.t(
                            "a",
                            &[CSS_PAGINATOR_ITEM, CSS_PAGINATOR_ITEM_PREV],
                            &[(
                                "href",
                                match (i - 1, first_path) {
                                    (0, Some(p)) => reverse(path_base).join(p),
                                    _ => Path::new(&format!("{}.html", i - 1 + 1)).to_owned(),
                                }
                                .to_str()
                                .unwrap(),
                            )],
                        )
                        .write("Previous")
                        .et("a");
                    }
                    if i + 1 < pages {
                        let next_url = format!("{}.html", i + 1 + 1);
                        seg.t(
                            "a",
                            &[CSS_PAGINATOR_ITEM, CSS_PAGINATOR_ITEM_NEXT],
                            &[(
                                "href",
                                match (i, first_path) {
                                    (0, Some(_)) => Cow::Owned(path_base.join(&next_url)),
                                    _ => Cow::Borrowed(Path::new(&next_url)),
                                }
                                .to_str()
                                .unwrap(),
                            )],
                        )
                        .write("Next")
                        .et("a");
                    }
                    seg.et("div");
                }
                seg.render(context, None, out);
            },
        );
    }
}
