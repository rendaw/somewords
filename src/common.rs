use crate::ci::ci_commit_file_url;
use crate::segments::common::WritableSegment;
use crate::segments::plain::PlainSegment;
use chrono::{DateTime, FixedOffset};
use slug::slugify;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fs;
use std::path::{Path, PathBuf};
use std::rc::Rc;

pub(crate) const CSS_SYNTAX_PREFIX: &'static str = "syntax-";
pub(crate) const CSS_BLOCK: &'static str = "block";
pub(crate) const CSS_ELEMENT_TITLE: &'static str = "title";
pub(crate) const CSS_ELEMENT_TITLE_TIME: &'static str = "time";
pub(crate) const CSS_ELEMENT_TITLE_AUTHOR: &'static str = "author";
pub(crate) const CSS_ELEMENT_HEADER_INDICES: &'static str = "indices";
pub(crate) const CSS_ELEMENT_HEADER_INDICES_ITEM: &'static str = "index";
pub(crate) const CSS_ELEMENT_HEADER_TEXT: &'static str = "text";
pub(crate) const CSS_LINK_SOURCE: &'static str = "link-source";
pub(crate) const CSS_LINK_LICENSE: &'static str = "link-license";
pub(crate) const CSS_SECTION_HEADER: &'static str = "header";
pub(crate) const CSS_SECTION_BODY: &'static str = "body";
pub(crate) const CSS_SECTION_FOOTER: &'static str = "footer";
pub(crate) const CSS_HEADER_LINK: &'static str = "header-link";
pub(crate) const CSS_HEADER_LINK_HOME: &'static str = "link-home";
pub(crate) const CSS_HEADER_LINK_POSTS: &'static str = "link-posts";
pub(crate) const CSS_HEADER_LINK_AUTHORS: &'static str = "link-authors";
pub(crate) const CSS_HEADER_LINK_TAGS: &'static str = "link-tags";
pub(crate) const CSS_HEADER_LINK_PIN: &'static str = "link-pin";
pub(crate) const CSS_HEADER_LINK_PIN_PREFIX: &'static str = "link-pin-";
pub(crate) const CSS_HEADER_BLOG_TITLE: &'static str = "blog-title";
pub(crate) const CSS_TAGS_LIST: &'static str = "doc-tags-list";
pub(crate) const CSS_TAGS_LIST_ITEM_LINK: &'static str = "doc-tags-item";
pub(crate) const CSS_DOC_LIST: &'static str = "doc-items";
pub(crate) const CSS_DOC_LIST_ITEM: &'static str = "doc-item";
pub(crate) const CSS_LIST: &'static str = "list-list";
pub(crate) const CSS_LIST_ITEM_LINK: &'static str = "list-link";
pub(crate) const CSS_PAGINATOR: &'static str = "paginator";
pub(crate) const CSS_PAGINATOR_ITEM: &'static str = "paginator-link";
pub(crate) const CSS_PAGINATOR_ITEM_PREV: &'static str = "paginator-prev";
pub(crate) const CSS_PAGINATOR_ITEM_NEXT: &'static str = "paginator-next";
pub(crate) const CSS_PAGE_SLUG_PREFIX: &'static str = "page-slug-";
pub(crate) const CSS_PAGE_PAGE: &'static str = "page-type-page";
pub(crate) const CSS_PAGE_PIN: &'static str = "page-pin";
pub(crate) const CSS_PAGE_DOC: &'static str = "page-type-doc";
pub(crate) const CSS_PAGE_INDEX: &'static str = "page-type-index";
pub(crate) const CSS_PAGE_TAGS: &'static str = "page-type-tags";
pub(crate) const CSS_PAGE_TAGS_PAGE: &'static str = "page-type-tags-page";
pub(crate) const CSS_PAGE_AUTHORS: &'static str = "page-type-authors";
pub(crate) const CSS_PAGE_AUTHORS_PAGE: &'static str = "page-type-authors-page";
pub(crate) const CSS_PAGE_POSTS_PAGE: &'static str = "page-type-posts-page";
pub(crate) const CSS_TOC: &'static str = "toc";
pub(crate) const CSS_TOC_ITEM: &'static str = "toc-item";
pub(crate) const CSS_TOC_LEVEL: &'static str = "toc-level";
pub(crate) const CSS_DEPTH: &'static str = "depth-";

pub(crate) struct TOC {
    pub(crate) text: String,
    pub(crate) plain_text: String,
    pub(crate) i: usize,
    pub(crate) children: Vec<Rc<RefCell<TOC>>>,
    pub(crate) parent: Option<Rc<RefCell<TOC>>>,
}

pub(crate) trait TOCIndices {
    fn indices(&self) -> Vec<usize>;
}

impl TOCIndices for Rc<RefCell<TOC>> {
    fn indices(&self) -> Vec<usize> {
        let mut indices: Vec<usize> = Vec::new();
        let mut at = self.clone();
        while at.as_ref().borrow().parent.is_some() {
            indices.push(at.as_ref().borrow().i);
            let next = at.as_ref().borrow().parent.as_ref().unwrap().clone();
            at = next;
        }
        indices.reverse();
        indices
    }
}

impl TOC {
    pub(crate) fn new(i: usize, parent: Option<Rc<RefCell<TOC>>>) -> Rc<RefCell<TOC>> {
        Rc::new(RefCell::new(TOC {
            text: "".to_string(),
            plain_text: "".to_string(),
            i,
            children: Vec::new(),
            parent,
        }))
    }

    pub(crate) fn slug(&self) -> String {
        return slugify(&self.plain_text);
    }
}

pub(crate) struct DocMeta {
    pub(crate) filename: String,
    pub(crate) repo_path: PathBuf,
    pub(crate) created: DateTime<FixedOffset>,
    pub(crate) updated: DateTime<FixedOffset>,
    pub(crate) id: String,
    pub(crate) author_name: String,
    pub(crate) author_email: String,
    pub(crate) title: RefCell<String>,
    pub(crate) plain_title: RefCell<String>,
    pub(crate) tags: RefCell<Vec<String>>,
    pub(crate) toc: Rc<RefCell<TOC>>,
}

impl DocMeta {
    pub(crate) fn slug(&self) -> String {
        slugify(self.plain_title.borrow().as_str())
    }

    pub(crate) fn out_filename(&self) -> String {
        if self.filename.starts_with("pin_") {
            format!("{}.html", slugify(&self.plain_title.borrow().as_str()))
        } else {
            format!("{}.html", self.id)
        }
    }

    pub(crate) fn out_url_filename(&self) -> String {
        if self.filename.starts_with("pin_") {
            format!("{}.html", slugify(&self.plain_title.borrow().as_str()))
        } else {
            format!("{}.html#{}", self.id, self.slug())
        }
    }
}

pub(crate) fn is_offsite(dest: &str) -> bool {
    dest.find("://")
        .and_then(|i| {
            dest.find("/")
                .and_then(|j| if j > i { Some(()) } else { None })
        })
        .is_some()
        || dest.starts_with("//")
}

pub(crate) fn write_doc_title(seg: &mut PlainSegment, doc: &DocMeta, doc_page: bool) {
    seg.t("span", &[CSS_ELEMENT_TITLE_AUTHOR], &[]);
    if doc.author_email.len() > 0 {
        seg.t(
            "a",
            &[],
            &[("href", &format!("mailto:{}", &doc.author_email))],
        );
    }
    seg.write_content(&doc.author_name);
    if doc.author_email.len() > 0 {
        seg.et("a");
    }
    seg.et("span");

    let source_link = ci_commit_file_url(doc.repo_path.to_str().unwrap());
    match (&source_link, &doc_page) {
        (Some(source_link), true) => {
            seg.t("a", &[CSS_ELEMENT_TITLE_TIME], &[("href", source_link)]);
        }
        _ => (),
    };
    let time_format = "%Y, %B %-d";
    seg.t(
        "time",
        if source_link.is_some() {
            &[]
        } else {
            &[CSS_ELEMENT_TITLE_TIME]
        },
        &[("datetime", &doc.created.to_rfc3339())],
    )
    .write_content(&if doc.created.eq(&doc.updated) || !doc_page {
        doc.created.format(time_format).to_string()
    } else {
        format!("Upd. {}", &doc.updated.format(time_format).to_string())
    })
    .et("time");
    if source_link.is_some() && doc_page {
        seg.et("a");
    }
}

pub(crate) fn copy_missing(context: &BlogContext, source_repo_path: &Path) {
    let source_path = source_repo_path.strip_prefix(&context.source_dir).unwrap();
    let out_path = context.output_dir.join(&source_path);
    if !fs::metadata(&out_path).is_ok() {
        fs::copy(context.repo_dir.join(&source_repo_path), &out_path).unwrap();
    }
}

pub(crate) struct BlogContext<'a> {
    pub(crate) doc_lookup: HashMap<String, &'a DocMeta>,
    pub(crate) source_dir: PathBuf,
    pub(crate) repo_dir: PathBuf,
    pub(crate) output_dir: PathBuf,
    pub(crate) license_url: Option<String>,
    pub(crate) title: String,
    pub(crate) pin: Vec<DocMeta>,
    pub(crate) index: Option<DocMeta>,
    pub(crate) tags: HashMap<String, Vec<&'a DocMeta>>,
    pub(crate) authors: HashMap<String, Vec<&'a DocMeta>>,
}
