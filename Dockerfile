FROM rust:1-buster AS build
ADD . /src
WORKDIR src
RUN cargo build --release

FROM debian:buster
RUN apt-get update && apt-get install -y git nodejs npm build-essential && rm -rf /var/lib/apt/lists/*
COPY --from=build /src/target/release/somewords /bin/somewords