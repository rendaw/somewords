#!/usr/bin/env python3
import argparse
import subprocess
import re
import sys
import toml

parser = argparse.ArgumentParser()
parser.add_argument(
    "-p", "--part", choices=["major", "minor", "patch"], default="patch"
)
args = parser.parse_args()

ver_regex = "([0-9]+\\.[0-9]+\\.[0-9]+)"

with open("Cargo.toml", "r") as cargo_f:
    cargo = toml.load(cargo_f)

ver = list(map(int, cargo["package"]["version"].split(".")))
ver[dict(major=0, minor=1, patch=2)[args.part]] += 1
ver = "{}.{}.{}".format(*ver)

confirm = input("Release [{}]? (y/) ".format(ver))
if confirm != "y":
    sys.exit(1)

cargo["package"]["version"] = ver
with open("Cargo.toml", "w") as cargo_f:
    toml.dump(cargo, cargo_f)

with open("readme.md", "r+", encoding="utf-8") as readme:
    text = readme.read()
    text = re.sub("somewords:{}".format(ver_regex), "somewords:{}".format(ver), text)
    readme.seek(0)
    readme.truncate(0)
    readme.write(text)

subprocess.check_call(["git", "commit", "-a", "-m", "Release {}".format(ver)])
subprocess.check_call(["git", "tag", ver])
subprocess.check_call(["git", "push", "--atomic", "origin", "master", ver])
